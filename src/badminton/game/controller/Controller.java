package badminton.game.controller;

import badminton.game.gui.Gui;
import badminton.game.models.Match;
import badminton.game.models.Player;
import badminton.game.models.Set;
import badminton.game.services.Services;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Controller {
    private Gui gui;
    private Services services;
    private static Scanner scanner = new Scanner(System.in);

    public Controller() {
        this.gui = new Gui();
        this.gui.setController(this);
    }

    public void setServices(Services services) {
        this.services = services;
    }

    public List<Player> createPlayersController() {
        gui.createFirstPlayerMessage();
        String firstPlayerName = scanner.next();
        Player firstPlayer = services.createPlayer(firstPlayerName);

        gui.createSecondPlayerMessage();
        String secondPlayerName = scanner.next();
        Player secondPlayer = services.createPlayer(secondPlayerName);

        ArrayList<Player> players = new ArrayList<>();
        players.add(firstPlayer);
        players.add(secondPlayer);
        return players;
    }

    public void inGame(Set set) {
        gui.currentActionInformation(set);
        gui.displayPlayers(set);

    }

    public void whoWonAction(Set set) {
        int userOutput = userOutput();
        if (userOutput == 1 | userOutput == 2) {
            services.setWhoWonAction(set, userOutput);
        } else {
            gui.displayError();
            gui.displayPlayers(set);
        }
    }

    public void playerServiceDecission(Player playerWhoWonAction) {
        gui.displayServiceSideDecisions(playerWhoWonAction);
    }

    public void userServiceSideDecission(Player playerWhoWonAction) {
        int userOutput = userOutput();
        if (userOutput == 1 | userOutput == 2) {
            services.playerChooseServiceSide(playerWhoWonAction, userOutput);
        } else {
            gui.displayError();
            gui.displayServiceSideDecisions(playerWhoWonAction);
        }
    }

    public void drawDecision(Set set, int expandedSetTo, Player playerWhichReachFirstSetBall) {
        gui.displaySetRulesDecisions(set, expandedSetTo, playerWhichReachFirstSetBall);
    }

    public void userDecisionAfterDraw(Set set, int expandedSetTo, Player playerWhichReachFirstSetBall) {
        int userOutput = userOutput();

        if (userOutput == 1 | userOutput == 2) {
            services.setNewRulesForSet(set, userOutput);
        } else {
            gui.displayError();
            gui.displaySetRulesDecisions(set, expandedSetTo, playerWhichReachFirstSetBall);
        }
    }

    private int userOutput() {
        try {
            return scanner.nextInt();
        } catch (InputMismatchException exc) {
            if (!scanner.hasNextInt()) {
                scanner.next();
            }
            return 0;
        }
    }

    public void winnerOfSet(Set set) {
        gui.displayWinnerOfSet(set);
    }

    public void winnerOfMatch(Match match) {
        gui.displayWinnerOfMatch(match);
    }

    public void showMessageBeforeStartSet(int size) {
        gui.showMessageBeforeStartSet(size);
    }
}
