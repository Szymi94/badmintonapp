package badminton.game.gui;

import badminton.game.controller.Controller;
import badminton.game.models.Match;
import badminton.game.models.Player;
import badminton.game.models.Set;

public class Gui {
    private Controller controller;

    public void setController(Controller controller) {
        this.controller = controller;
    }

    public void createFirstPlayerMessage() {
        System.out.println("Witaj w symulatorze gry badmintona!\n");
        System.out.println("Podaj imię pierwszego gracza.");
    }

    public void createSecondPlayerMessage() {
        System.out.println("Podaj imię drugiego gracza.");
    }

    public void currentActionInformation(Set set) {
        System.out.println("Gra w toku");
        System.out.println(set.actualScore());
        System.out.println("Serwuje " + set.getServicePlayer().toString() + " z " + set.getServiceSide() + " strony.");
        System.out.println("---------------");
    }

    public void displayPlayers(Set set) {
        System.out.println("Kto zdobył punkt?");
        System.out.println("1. " + set.getPlayerOne().toString() + "\n" + "2. " + set.getPlayerTwo().toString());
        controller.whoWonAction(set);
    }

    public void displayServiceSideDecisions(Player playerWhoWonAction) {
        System.out.println("---------------------------------------");
        System.out.println("Gracz: " + playerWhoWonAction.toString() + " decyduje z którego pola serwuje:\n" +
                "1. Prawe pole\n" +
                "2. Lewe pole");
        controller.userServiceSideDecission(playerWhoWonAction);
    }

    public void displaySetRulesDecisions(Set set, int expandedSetTo, Player playerWhichReachFirstSetBall) {
        System.out.println(playerWhichReachFirstSetBall.toString() + " decyduje o dalszej rozgrywce seta.\n" +
                "1. Gramy do " + set.getPointsToReach() + "\n" +
                "2. Gramy do " + expandedSetTo + " lub do dwóch punktów przewagi");
        controller.userDecisionAfterDraw(set, expandedSetTo, playerWhichReachFirstSetBall);
    }

    public void displayWinnerOfSet(Set set) {
        System.out.println("Seta wygrał: " + set.getWhoWonSet() + "\n" + set.getFinalScore());
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void displayWinnerOfMatch(Match match) {
        System.out.println(match.getFinalScore());
        goodByeMessage();
    }

    public void goodByeMessage() {
        System.out.println("Do zobaczenia w następnej rozgrywce!");
    }

    public void displayError() {
        System.out.println("Podane dane są nieprawidłowe, wybierz jeden z podanych indeksów");
    }

    public void showMessageBeforeStartSet(int size) {
        String message = size == 3 ? "Rozpoczynamy tie break!" : "Rozpoczynamy set " + size;
        System.out.println(message);
    }
}
