package badminton.game.services;

import badminton.game.constants.ServiceSide;
import badminton.game.controller.Controller;
import badminton.game.models.Player;
import badminton.game.models.Set;
import badminton.game.set_rules.SetRules;

public class Services {
    private Controller controller;

    public Services(Controller controller) {
        this.controller = controller;
    }

    public Player createPlayer(String playerName) {
        return new Player(playerName);
    }

    public void setWhoWonAction(Set set, int userOutput) {
        Player playerWhoWonAction = set.getUserWhoWonAction(userOutput);
        if (playerWhoWonAction.equals(set.getServicePlayer())) {
            addPointToUser(playerWhoWonAction);
            changePlayerServiceSide(playerWhoWonAction);
        } else {
            set.setServicePlayer(playerWhoWonAction);
            controller.playerServiceDecission(playerWhoWonAction);
        }
    }

    private void changePlayerServiceSide(Player playerWhoWonAction) {
        playerWhoWonAction.setServiceSide(playerWhoWonAction.getServiceSide() == ServiceSide.PRAWA ? ServiceSide.LEWA : ServiceSide.PRAWA);
    }

    private void addPointToUser(Player playerWhoWonAction) {
        playerWhoWonAction.setPoints(playerWhoWonAction.getPoints() + 1);
    }

    public void playerChooseServiceSide(Player playerWhoWonAction, int userOutput) {
        if (userOutput == 1) {
            playerWhoWonAction.setServiceSide(ServiceSide.PRAWA);
        } else {
            playerWhoWonAction.setServiceSide(ServiceSide.LEWA);
        }
    }

    public void setNewRulesForSet(Set set, int userOutput) {
        if (userOutput == 2 && set.getPointsToReach() == SetRules.defaultSet) {
            set.setPointsToReach(SetRules.winningPointInDefaultSet);
        } else if (userOutput == 2 && set.getPointsToReach() == SetRules.tieBreak){
            set.setPointsToReach(SetRules.winningPointInTieBreak);
        }
    }
}
