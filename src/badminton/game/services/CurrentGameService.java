package badminton.game.services;

import badminton.game.constants.ServiceSide;
import badminton.game.controller.Controller;
import badminton.game.models.Match;
import badminton.game.models.Player;
import badminton.game.models.Set;
import badminton.game.set_rules.SetRules;

import java.util.HashMap;

public class CurrentGameService {
    private Controller controller;

    public CurrentGameService(Controller controller) {
        this.controller = controller;
    }

    //Set validators with different possibilities

    public void checkIfNoDraw(Set set) {
        if (drawPossibilities(set) && set.getPlayerOne().getPoints() == set.getPlayerTwo().getPoints() && !set.isPointToReachSet()) {
            Player playerWhichReachFirstSetBall = set.getServicePlayer().equals(set.getPlayerOne()) ?
                    set.getPlayerTwo() : set.getPlayerOne();
            int expandedSetTo = checkIsTiebreak(set);
            set.setPointToReachSet(true);
            controller.drawDecision(set, expandedSetTo, playerWhichReachFirstSetBall);
        }
    }

    private boolean drawPossibilities(Set set) {
        int reachedPointsByUser = set.getServicePlayer().getPoints();
        int pointsToGetInSet = set.getPointsToReach();
        return (pointsToGetInSet == SetRules.defaultSet && reachedPointsByUser == SetRules.drawInNormalSet) ||
                (pointsToGetInSet == SetRules.tieBreak && reachedPointsByUser == SetRules.drawInTieBreak);
    }

    private int checkIsTiebreak(Set set) {
        return set.getPointsToReach() == SetRules.defaultSet ? SetRules.winningPointInDefaultSet : SetRules.winningPointInTieBreak;
    }

    // Reset data after end set

    public void setSetData(Set set) {
        set.setWhoWonSet(checkWhoWon(set));
        setScore(set);
        resetPlayerStats(set);
    }

    private void resetPlayerStats(Set set) {
        Player playerOne = set.getPlayerOne();
        Player playerTwo = set.getPlayerTwo();
        playerOne.setPoints(0);
        playerOne.setServiceSide(ServiceSide.PRAWA);
        playerTwo.setPoints(0);
        playerTwo.setServiceSide(ServiceSide.PRAWA);
    }

    private void setScore(Set set) {
        HashMap<String, Integer> score = new HashMap<>();
        score.put(set.getPlayerOne().getName(), set.getPlayerOne().getPoints());
        score.put(set.getPlayerTwo().getName(), set.getPlayerTwo().getPoints());
        set.setScore(score);
    }

    private Player checkWhoWon(Set set) {
        if (set.getPlayerOne().getPoints() == set.getPointsToReach() ||
                set.getPlayerOne().getPoints() - 2 == set.getPlayerTwo().getPoints()) {
            return set.getPlayerOne();
        } else {
            return set.getPlayerTwo();
        }
    }

    // Checking who should serve now.

    public void checkWhoServes(Set set, Player player) {
        if (player != null) {
            set.setServicePlayer(player);
        } else {
            lotteryWhoStart(set);
        }
    }

    private void lotteryWhoStart(Set set) {
        int whichPlayer = (int) (Math.random() * 2);
        if (whichPlayer == 1) {
            set.setServicePlayer(set.getPlayerOne());
        } else {
            set.setServicePlayer(set.getPlayerTwo());
        }
    }

    public Player checkWhoWonLastSet(Match match) {
        int lastSetIndex = match.getSets().size() == 1 ? 1 : 2;
        return match.getSets().get(match.getSets().size() - lastSetIndex).getWhoWonSet();
    }

    public Set createNewSet(Match match) {
        Player playerOneInMatch = match.getSets().get(0).getPlayerOne();
        Player playerTwoInMatch = match.getSets().get(0).getPlayerTwo();
        return new Set(playerOneInMatch, playerTwoInMatch);
    }
}
