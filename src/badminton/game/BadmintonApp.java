package badminton.game;

import badminton.game.controller.Controller;
import badminton.game.models.Match;
import badminton.game.models.Player;
import badminton.game.models.Set;
import badminton.game.services.CurrentGameService;
import badminton.game.services.Services;
import badminton.game.set_rules.SetRules;

import java.util.List;

public class BadmintonApp {
    private static Match match;
    private static Controller controller;
    private CurrentGameService currentGameService;

    public static void main(String[] args) {
        controller = new Controller();
        Services services = new Services(controller);
        controller.setServices(services);
        BadmintonApp badmintonApp = new BadmintonApp();
        badmintonApp.setCurrentGameService(new CurrentGameService(controller));

        boolean isNotFinishedMatch = true;
        List<Player> playersController = controller.createPlayersController();
        Set set = badmintonApp.startMatch(playersController);

        do {
            set.setPointsToReach(badmintonApp.getHowManyPointsToReachInSet());
            badmintonApp.whoServes(set);
            controller.showMessageBeforeStartSet(match.getSets().size());
            while (!set.is_finished()) {
                controller.inGame(set);
                badmintonApp.checkIfNoDraw(set);
            }

            badmintonApp.setSetData(set);

            if (badmintonApp.isSomeoneWonMatch()) {
                isNotFinishedMatch = false;
            } else {
                controller.winnerOfSet(set);
                set = badmintonApp.startNewSet();
            }

        } while (isNotFinishedMatch);

        match.setWhoWonTheMatch(set.getWhoWonSet());
        controller.winnerOfMatch(match);
    }

    private void setCurrentGameService(CurrentGameService currentGameService) {
        this.currentGameService = currentGameService;
    }

    private void setSetData(Set set) {
        currentGameService.setSetData(set);
    }

    private void whoServes(Set set) {
        Player player = checkWhoWonLastSet();
        currentGameService.checkWhoServes(set, player);
    }

    private Player checkWhoWonLastSet() {
        return currentGameService.checkWhoWonLastSet(match);
    }

    private void checkIfNoDraw(Set set) {
        currentGameService.checkIfNoDraw(set);
    }

    private Set startNewSet() {
        Set set = currentGameService.createNewSet(match);
        match.startSet(set);
        return set;
    }

    private Set startMatch(List<Player> players) {
        match = new Match();
        Set set = new Set(players.get(0), players.get(1));
        match.startSet(set);
        return set;
    }

    private boolean isSomeoneWonMatch() {
        return match.getSets().size() > 1 && !match.checkIfDraw();
    }

    private int getHowManyPointsToReachInSet() {
        return match.getSets().size() == 3 ? SetRules.tieBreak : SetRules.defaultSet;
    }
}
