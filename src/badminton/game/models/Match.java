package badminton.game.models;


import java.util.ArrayList;

public class Match {
    private Player whoWonTheMatch;
    private ArrayList<Set> sets = new ArrayList<>();

    public void startSet(Set set) {
        sets.add(set);
    }

    public String getFinalScore() {
        return "Mecz pomiędzy " + getPlayers() + "\nwygrał: " + whoWonTheMatch + "\n" +
                getSetScores();
    }

    public void setWhoWonTheMatch(Player whoWonTheMatch) {
        this.whoWonTheMatch = whoWonTheMatch;
    }

    public ArrayList<Set> getSets() {
        return sets;
    }

    private String getSetScores() {
        StringBuilder builder = new StringBuilder();
        for (Set set : sets) {
            builder.append(set.getFinalScore() + "\n");
        }
        return builder.toString();
    }

    private String getPlayers() {
        return sets.get(0).getPlayers();
    }

    public boolean checkIfDraw() {
        Player playerWhoWonLastSet = sets.get(sets.size() - 1).getWhoWonSet();
        return sets.size() == 2 && !sets.stream().allMatch(set -> set.getWhoWonSet().equals(playerWhoWonLastSet));
    }
}
