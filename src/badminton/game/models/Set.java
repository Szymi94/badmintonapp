package badminton.game.models;

import badminton.game.constants.ServiceSide;
import badminton.game.set_rules.SetRules;

import java.util.FormatFlagsConversionMismatchException;
import java.util.HashMap;

public class Set {
    private Player playerOne;
    private Player playerTwo;
    private Player whoWonSet;
    private Player servicePlayer;
    private HashMap<String, Integer> score;
    private int pointsToReach;
    private boolean isPointToReachSet = false;

    public boolean isPointToReachSet() {
        return isPointToReachSet;
    }

    public void setPointToReachSet(boolean pointToReachSet) {
        isPointToReachSet = pointToReachSet;
    }

    public int getPointsToReach() {
        return pointsToReach;
    }

    public void setPointsToReach(int pointsToReach) {
        this.pointsToReach = pointsToReach;
    }

    public Set(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public Player getWhoWonSet() {
        return whoWonSet;
    }

    public void setWhoWonSet(Player whoWonSet) {
        this.whoWonSet = whoWonSet;
    }

    public Player getServicePlayer() {
        return servicePlayer;
    }

    public void setServicePlayer(Player servicePlayer) {
        this.servicePlayer = servicePlayer;
    }

    public void setScore(HashMap<String, Integer> score) {
        this.score = score;
    }

    public boolean is_finished() {
        return SomeoneWon();
    }

    private boolean SomeoneWon() {
        boolean reachWinningPoint = this.playerOne.getPoints() == this.pointsToReach || this.playerTwo.getPoints() == this.pointsToReach;
        return reachWinningPoint || checkDifferenceOfTwoPoints();
    }

    private boolean checkDifferenceOfTwoPoints() {
        return this.pointsToReach == SetRules.winningPointInDefaultSet && someOneHasTwoPointsMore();
    }

    private boolean someOneHasTwoPointsMore() {
        return playerOne.getPoints() - 2 == getPlayerTwo().getPoints() || playerTwo.getPoints() - 2 == playerOne.getPoints();
    }

    public String actualScore() {
        return this.playerOne + " | " + this.playerTwo + "\n" + alignText() +
                this.playerOne.getPoints() + " : " + this.playerTwo.getPoints();
    }

    private String alignText() {
        try {
            return String.format("%1$" + (this.playerOne.getName().length() - 1) + "s", " ");
        } catch (FormatFlagsConversionMismatchException exc) {
            return "";
        }
    }

    public String getServiceSide() {
        if (servicePlayer.getServiceSide() == ServiceSide.LEWA) {
            return "lewej";
        } else {
            return "prawej";
        }
    }

    public Player getUserWhoWonAction(int userOutput) {
        if (userOutput == 1) {
            return this.getPlayerOne();
        } else {
            return this.getPlayerTwo();
        }
    }

    public String getPlayers() {
        return this.playerOne + " vs " + this.playerTwo;
    }

    public String getFinalScore() {
        return score.get(playerOne.getName()) + " : " + score.get(playerTwo.getName());
    }
}
