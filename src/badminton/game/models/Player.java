package badminton.game.models;

import badminton.game.constants.ServiceSide;

import java.util.Objects;

public class Player {
    private String name;
    private int points;
    private ServiceSide serviceSide;

    public Player(String name) {
        this.name = name;
        this.points = 0;
        this.serviceSide = ServiceSide.PRAWA; // defaultowo
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public ServiceSide getServiceSide() {
        return serviceSide;
    }

    public void setServiceSide(ServiceSide serviceSide) {
        this.serviceSide = serviceSide;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return points == player.points &&
                name.equals(player.name) &&
                serviceSide == player.serviceSide;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, points, serviceSide);
    }
}
