package badminton.game.set_rules;

public class SetRules {
    public static int drawInNormalSet = 14;
    public static int drawInTieBreak = 10;
    public static int tieBreak = 11;
    public static int winningPointInTieBreak = 13;
    public static int defaultSet = 15;
    public static int winningPointInDefaultSet = 17;
}
