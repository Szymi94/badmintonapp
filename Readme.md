### Aplikacja do zliczania punktów podczas meczu badmintona 

#### Zasady użytkowania aplikacji
 1. Na początku należy podać imiona graczy ze sobą rywalizujących.
 2. Podczas gry należy odpowiadać na zadawane pytania przez aplikację używając odpowiednich liczb:
      - 1
      - 2
 3. W przypadku błędnej odpowiedzi, aplikacja poprosi o podanie prawidłowej wartości.
 
#### Zasady gry
 1. Gracze grają do 2 wygranych setów, do 15 punktów.
 2. W przypadku stanu gry 1:1 zostaje rozegrany decydujący set, tzw. tie break, do 11 punktów.
 3. Rozpoczynając grę gracz serwujący serwuje z prawej strony.
 4. Gracz zdobywa punkt tylko podczas wykonywania własnego serwisu.
 5. Gdy akcję wygra gracz odbierający to:
    - uzyskuję możliwość wybrania dowolnej strony z której chce serwować (Prawa lub Lewa)
    - uzyskuje możliwość zdobycia punktu w momencie gdy wygra akcję przy swoim serwisie.
 6. Gracz serwujący po każdej wygranej akcji zmienia stronę z której serwuje.
 7. Gdy wynik seta wyniesie 14:14 to gracz, który jako pierwszy zdobył 14 punkt decyduje:
    - czy rozgrywka może trwać do 17 punktów lub osiągnięcia 2 punktów przewagi przez jednego z graczy
    - czy o wygraniu seta decyduje 15 punkt.
 8. Gdy wynik seta w tie breaku wyniesie 10:10 to gracz, który jak pierwszy zdobył 10 punkt decyduje:
    - czy rozgrywka może trwać do 13 punktów lub 2 punktów przewagi przez jednego z graczy
    - czy o wygraniu seta decyduje 11 punkt.

